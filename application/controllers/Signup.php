<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signup extends CI_Controller {


	public function index()
	 {
		$this->load->view('signup');

	}

      public function admin_login()
       {
        $this->load->model("signup_model");
           //echo 'OK';
           $this->load->library('form_validation');

           $this->form_validation->set_rules("user_name", "user_name", 'required');
           $this->form_validation->set_rules("user_email", "user_email", 'required|valid_email');
           $this->form_validation->set_rules("password", "password", 'required');

           if($this->form_validation->run())
           {
                //true

                $data = array(
                     "user_name" =>$this->input->post("user_name"),
                     "user_email" =>$this->input->post("user_email"),
                     "password" =>$this->input->post("password")
                );
				$this->signup_model->insert_data($data);

		return redirect('admin');
	  }
	  else{
		  $this->load->view('signup');}
} }
