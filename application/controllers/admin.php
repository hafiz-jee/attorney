<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
   function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		if($this->session->userdata('user_email'))
			return redirect('admin/dashboard');

		$this->load->view('admin/login');
}
	public function dashboard()
	{
		if($this->session->userdata('user_email')){
			$this->load->view('admin/index');
			//return redirect('admin/dashboard');
		}else{
			return redirect('admin/login');
		}
		/*if($this->session->userdata('user_email'))
			return redirect('admin/dashboard');
		$this->load->view('admin/index');*/
	}

	public function login()
	{
    $data['error'] ="Invalid Username or Pass";
		$this->form_validation->set_rules('username','User Name', 'required|valid_email');
		$this->form_validation->set_rules('password','Password', 'required');
		if($this->form_validation->run('admin_login')){
			$email = $this->input->post('username');
			$pass  = $this->input->post('password');
			$this->load->model('users_model');
			if($this->users_model->login($email , $pass)){
				$this->session->set_userdata('user_email',$email);
				 return redirect('admin/dashboard');
			}else{


$this->load->view('admin/login',$data);
      //  echo "invalid userName or Password";
			//	$this->load->view('admin/login');
			}
		}else{
			$this->load->view('admin/login');
		}

		// echo $email;
		// echo $pass;
	}

	public function logout()
	{
	//	$this->session->sess_destroy();
	$this->session->unset_userdata('user_email');
	  return redirect('admin');

	}




}
