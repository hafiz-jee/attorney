<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {
	public function index()
	{
		$this->load->view('user_login');
	}
	public function addclient()
	{
		$this->load->view('addclient_view');
	}

	public function opencase()
	{
		$this->load->view('opencase');
	}
	public function funded()
	{
		$this->load->view('Funded');
	}
	public function Help()
	{
		$this->load->view('help');
	}
	public function Signup()
	{
		$this->load->view('signup');
	}
}
