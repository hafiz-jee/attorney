<?php include('login_header.php');?>
   <div class="page login-page">
      <div class="container">
        <div class="form-outer text-center d-flex align-items-center">
          <div class="form-inner">
          <?php  if (isset($error)){
    echo "<div class='alert alert-danger'>$error</div>";
}?>
            <div class="logo text-uppercase"><span>Admin</span><strong class="text-primary">Login</strong></div>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>
            <form id="login-form" method="post" <?php echo form_open('admin/login');?>
              <div class="form-group">
                <label for="login-username" class="label-custom">User Name</label>
                <input id="login-username" type="text" name="username" >
                <?php echo form_error("username","<p class='text-danger'>","</p>");?>
              </div>
              <div class="form-group">
                <label for="login-password" class="label-custom">Password</label>
                <input id="login-password" type="password" name="password" >
                 <?php echo form_error("password","<p class='text-danger'>","</p>");?>
              </div>
              <button class="btn btn-lg btn-primary" type="submit">Login</button>
              <!-- This should be submit button but I replaced it with <a> for demo purposes-->
            </form><!--<a href="#" class="forgot-pass">Forgot Password?</a><small>Do not have an account? </small><a href="register.html" class="signup">Signup</a>-->
<a href="#" class="forgot-pass">Forgot Password?</a><small>Do not have an account? </small>

<a href="<?php echo site_url('users/Signup');?>" class="signup">Signup</a>

          </div>
          <div class="copyrights text-center">
            <p>Design by <a href="http://traximtech.com/" class="external">TRAXIM TECHNOLOGIES</a></p>
            <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
          </div>
        </div>
      </div>
    </div>
	<?php include('login_footer.php');?>
