<nav class="side-navbar">
      <div class="side-navbar-wrapper">
        <div class="sidenav-header d-flex align-items-center justify-content-center">
          <div class="sidenav-header-inner text-center"><img src="<?php echo base_url();?>assets/img/avatar-1.jpg" alt="person" class="img-fluid rounded-circle">
            <h2 class="h5 text-uppercase">Anderson Hardy</h2><span class="text-uppercase">Web Developer</span>
          </div>
          <div class="sidenav-header-logo"><a href="" class="brand-small text-center"> <strong>A</strong><strong class="text-primary">D</strong></a></div>
        </div>
        <div class="main-menu">
          <ul id="side-main-menu" class="side-menu list-unstyled">
            <li class="active"><a href="<?php echo site_url('admin/dashboard');?>"> <i class="icon-home"></i><span>Home</span></a></li>
            <li> <a href="<?php echo site_url('users/addclient');?>"><i class="icon-form"></i><span>Add Client</span></a></li>
            <li> <a href="<?php echo site_url('users/opencase');?>"> <i class="icon-interface-windows"></i><span>Open Case
                    </span></a></li>
              <li> <a href="<?php echo site_url('users/funded');?>"> <i class="icon-interface-windows"></i><span>Funded
                    </span></a></li>
              <li> <a href="<?php echo site_url('users/Help');?>"> <i class="icon-interface-windows"></i><span>Help
                    </span></a></li>
          </ul>
        </div>
      </div>
    </nav>
