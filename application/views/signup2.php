 <html>  
 <head>  
   <title>Sign up</title>  
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />  
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>  
 </head>  
 <body>  
<div class="container">

<?php echo form_open('signup/admin_login',['class'=>'form-horizontal']);?>
<!--<form class="form-horizontal">-->
  <fieldset>
  <?php if($error = $this->session->flashdata('login_failed')){ ?>
  <div class="form-group">
	  <label for="inputEmail" class="col-lg-2 control-label"></label>
		<div class="col-lg-5">
			<div class="alert alert-dismissible alert-danger">
			  <button type="button" class="close" data-dismiss="alert">&times;</button>
			  <strong>Oh snap!</strong> <?php echo $error;?>.
			</div>
		</div>
	</div>
	<?php }?>
    <center><legend>Register New User</legend></center>
	 <div class="form-group">
      <label for="inputEmail" class="col-lg-2 control-label">First Name</label>
      <div class="col-lg-5">
	  <?php echo form_input(['name'=>'user_name','class'=>'form-control','placeholder'=>'user_name', 'value'=>set_value('user_name')]);?>
      </div>
	  <div class="col-lg-5">
	  <?php echo form_error("username","<p class='text-danger'>","</p>");?>
      </div>
    </div>
    <div class="form-group">
      <label for="inputEmail" class="col-lg-2 control-label">Email</label>
      <div class="col-lg-5">
	  <?php echo form_input(['name'=>'user_email','class'=>'form-control','placeholder'=>'Email', 'value'=>set_value('user_email')]);?>
      </div>
	  <div class="col-lg-5">
	  <?php echo form_error("user_email","<p class='text-danger'>","</p>");?>
      </div>
    </div>
    <div class="form-group">
      <label for="inputPassword" class="col-lg-2 control-label">Password</label>
      <div class="col-lg-5">
        <?php echo form_password(['name'=>'password','class'=>'form-control','placeholder'=>'Password', 'value'=>set_value('password')]);?>
      </div>
	  <div class="col-lg-5">
	  <?php echo form_error("password","<p class='text-danger'>","</p>");?>
      </div>
    </div>
    
    <div class="form-group">
      <div class="col-lg-10 col-lg-offset-2">
        <?php echo form_reset(['name'=>'reset','value'=>'Cancel','class'=>'btn btn-default']);?>
        <?php echo form_submit(['name'=>'submit','value'=>'Register','class'=>'btn btn-primary']);?>
      </div>
    </div>
  </fieldset>
</form>
</div>
      <script>  
      $(document).ready(function(){  
           $('.delete_data').click(function(){  
                var id = $(this).attr("id");  
                if(confirm("Are you sure you want to delete this?"))  
                {  
                     window.location="<?php echo base_url(); ?>main/delete_data/"+id;  
                }  
                else  
                {  
                     return false;  
                }  
           });  
      });  
      </script>  
 </div>  
 </body>  
 </html>  