<?php include('admin/login_header.php');?>
  <body>
    <div class="page login-page">
      <div class="container">
        <div class="form-outer text-center d-flex align-items-center">
          <div class="form-inner">
            <div class="logo text-uppercase"><span>Sign</span><strong class="text-primary">up</strong></div>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>
            <form id="register-form" <?php echo form_open('signup/admin_login');?>
              <div class="form-group">
                <label for="register-username" class="label-custom">User Name</label>
                <input id="register-username" type="text" name="user_name" >
               <?php echo form_error("username","<p class='text-danger'>","</p>");?>

              </div>
              <div class="form-group">
                <label for="register-email" class="label-custom">Email Address      </label>
                <input id="register-email" type="email" name="user_email" >
                 <?php echo form_error("user_email","<p class='text-danger'>","</p>");?>
              </div>
              <div class="form-group">
                <label for="register-passowrd" class="label-custom">password        </label>
                <input id="register-passowrd" type="password" name="password" >
                  <?php echo form_error("password","<p class='text-danger'>","</p>");?>
              </div>
              <div class="terms-conditions d-flex justify-content-center">
                <input id="license" type="checkbox" class="form-control-custom">
                <label for="license">Agree the terms and policy</label>
              </div>
              <input type="submit" value="Register" class="btn btn-primary">
            </form><small>Already have an account? </small><a href="<?php echo site_url('admin');?>" class="signup">Login</a>
          </div>
          <div class="copyrights text-center">
              <p>Design by <a href="http://traximtech.com/" class="external">TRAXIM TECHNOLOGIES</a></p>
            <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
          </div>
        </div>
      </div>
    </div>
  	<?php include('admin/login_footer.php');?>
